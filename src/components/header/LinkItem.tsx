import { linkItemProps } from "@/types/components";
import { Box, Text } from "@chakra-ui/react";
import { AnimatePresence, motion } from "framer-motion";
import Link from "next/link";
import { useRouter } from "next/router";

function LinkItem({ title, url }: linkItemProps) {
  const router = useRouter();

  return (
    <Box as={motion.div} whileHover={{ scale: 1.1 }}>
      <Link href={url}>
        <Text>{title}</Text>
        <AnimatePresence initial={false}>
          {router.asPath.includes(url) && (
            <Box
              as={motion.div}
              layoutId="linkHeader"
              w="full"
              h="1"
              bg="white"
              mt="1"
            />
          )}
        </AnimatePresence>
      </Link>
    </Box>
  );
}

export default LinkItem;
