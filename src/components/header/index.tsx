import { ReactNode, useState } from "react";
import { Flex } from "@chakra-ui/react";
import Image from "next/image";
import Link from "next/link";
import {
  AnimatePresence,
  motion,
  useMotionValueEvent,
  useScroll,
} from "framer-motion";
import { scrollValue } from "@/types/constant";
import LinkItem from "./LinkItem";

function Header(): ReactNode {
  const [scrollPosition, setScrollPosition] = useState<number>(0);
  const [scrollDirection, setScrollDirection] = useState<scrollValue>("bottom");
  const { scrollYProgress } = useScroll();

  useMotionValueEvent(scrollYProgress, "change", () => {
    setScrollDirection(scrollYProgress.getVelocity() > 0 ? "up" : "bottom");
    setScrollPosition(scrollYProgress.get());
  });

  return (
    <AnimatePresence initial={false}>
      {scrollDirection === "bottom" && (
        <Flex
          as={motion.div}
          initial={{ clipPath: "inset(0% 0% 100% 0%)" }}
          animate={{
            backdropFilter: "blur(4px)",
            background: `rgb(255, 102, 0, ${scrollPosition > 0 ? "0.7" : "1"})`,
            clipPath: "inset(0% 0% 0% 0%)",
            transition: { ease: "easeOut", duration: 0.2 },
          }}
          exit={{
            clipPath: "inset(0% 0% 100% 0%)",
            transition: { ease: "easeOut", duration: 0.2 },
          }}
          position="fixed"
          zIndex="99"
          w="full"
          py="6"
          px="24"
          alignItems="center"
          justifyContent="space-between"
          color="white"
        >
          <Link href="/">
            <Image
              alt="suitmedia"
              src="/suitmedia.png"
              style={{ objectFit: "contain" }}
              height={0}
              width={100}
            />
          </Link>
          <Flex gap="6">
            <LinkItem title="Work" url="/work" />
            <LinkItem title="About" url="/about" />
            <LinkItem title="Services" url="/services" />
            <LinkItem title="Ideas" url="/ideas" />
            <LinkItem title="Careers" url="/careers" />
            <LinkItem title="Contact" url="/contact" />
          </Flex>
        </Flex>
      )}
    </AnimatePresence>
  );
}

export default Header;
