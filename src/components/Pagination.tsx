import { paginationProps } from "@/types/components";
import { Button, Flex } from "@chakra-ui/react";
import { useRouter } from "next/router";

function Pagination({ current_page, last_page, children }: paginationProps) {
  const router = useRouter();
  function handleChangePage(pageNumber: number) {
    router.replace({
      pathname: "",
      query: { ...router.query, "page[number]": pageNumber },
    });
  }

  const inactiveButtonStyle = { size: "sm", variant: "ghost" };

  return (
    <Flex flexDirection="column" gap="16">
      {children}
      <Flex justifyContent="center" gap="2">
        <Button
          {...inactiveButtonStyle}
          isDisabled={current_page === 1}
          onClick={() => handleChangePage(1)}
        >
          {"<<"}
        </Button>
        <Button
          {...inactiveButtonStyle}
          isDisabled={current_page === 1}
          onClick={() => handleChangePage(current_page - 1)}
        >
          {"<"}
        </Button>
        {current_page !== 1 && (
          <Button {...inactiveButtonStyle} onClick={() => handleChangePage(1)}>
            1
          </Button>
        )}
        {current_page - 1 > 1 && (
          <Button
            {...inactiveButtonStyle}
            onClick={() => handleChangePage(current_page - 1)}
          >
            {current_page - 1}
          </Button>
        )}
        <Button size="sm" bg="suitmedia.orange" colorScheme="orange">
          {current_page}
        </Button>
        {current_page + 1 < last_page && (
          <Button
            {...inactiveButtonStyle}
            onClick={() => handleChangePage(current_page + 1)}
          >
            {current_page + 1}
          </Button>
        )}
        {current_page !== last_page && (
          <Button
            {...inactiveButtonStyle}
            onClick={() => handleChangePage(last_page)}
          >
            {last_page}
          </Button>
        )}
        <Button
          {...inactiveButtonStyle}
          isDisabled={current_page === last_page}
          onClick={() => handleChangePage(current_page + 1)}
        >
          {">"}
        </Button>
        <Button
          {...inactiveButtonStyle}
          isDisabled={current_page === last_page}
          onClick={() => handleChangePage(last_page)}
        >
          {">>"}
        </Button>
      </Flex>
    </Flex>
  );
}

export default Pagination;
