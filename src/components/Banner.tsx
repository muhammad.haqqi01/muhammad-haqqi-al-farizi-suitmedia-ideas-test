import { ReactNode } from "react";
import { Flex, Text, Box, Heading } from "@chakra-ui/react";
import { bannerProps } from "@/types/components";

function Banner({ image }: bannerProps): ReactNode {
  return (
    <Flex
      position="relative"
      flexDirection="column"
      justifyContent="space-between"
    >
      <Flex
        bgImage={image}
        w="full"
        h="100vh"
        bgAttachment="fixed"
        bgSize="cover"
      >
        <Flex
          position="relative"
          zIndex="2"
          flexDirection="column"
          m="auto"
          color="white"
        >
          <Heading textAlign="center" fontWeight="regular" fontSize="5xl">
            Ideas
          </Heading>
          <Text>Where all our great things begin</Text>
        </Flex>
        <Box position="absolute" w="full" h="full" bg="black" opacity="0.4" />
        <Box
          position="absolute"
          bottom="0"
          borderLeft="100vw solid transparent"
          borderBottom="140px solid white"
        />
      </Flex>
    </Flex>
  );
}

export default Banner;
