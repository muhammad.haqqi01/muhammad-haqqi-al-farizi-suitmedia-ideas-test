import { dropdownProps } from "@/types/components";
import { dropdownValue } from "@/types/constant";
import { Box, Flex, SlideFade, Text, useDisclosure } from "@chakra-ui/react";
import { ReactNode, useState } from "react";
import { Icon } from "@iconify/react";

function Dropdown({
  title,
  initDropdownTitle,
  dropdownValue,
  onSelect,
}: dropdownProps): ReactNode {
  const { isOpen, onToggle } = useDisclosure();
  const [selectedValue, setSelectedValue] = useState<number | string>(
    initDropdownTitle ?? dropdownValue[0].title
  );

  function handleSelectFilter(item: dropdownValue) {
    setSelectedValue(item.title);
    onSelect(item.value);
    onToggle();
  }

  return (
    <Flex gap="4" alignItems="center">
      <Text fontWeight="medium">{title}: </Text>
      <Flex position="relative" flexDirection="column" minW="36">
        <Flex
          px="4"
          py="2"
          alignItems="center"
          justifyContent="space-between"
          border="1px"
          borderColor="blackAlpha.800"
          borderRadius="full"
          cursor="pointer"
          onClick={onToggle}
        >
          <Text>{selectedValue}</Text>
          <Box
            transform={isOpen ? "rotate(120deg)" : "initial"}
            transition="ease-out 0.2s"
          >
            <Icon
              icon="gravity-ui:triangle-down-fill"
              color="#202020"
              rotate={isOpen ? 2 : 0}
              width={12}
            />
          </Box>
        </Flex>
        <Box position="absolute" zIndex="99" top="12">
          <SlideFade in={isOpen} offsetY={"-8px"}>
            <Flex
              flexDirection="column"
              bg="white"
              border="1px"
              borderColor="blackAlpha.300"
              borderRadius="lg"
              boxShadow="xl"
            >
              {dropdownValue.map((item, idx) => (
                <Box
                  key={item.title}
                  px="4"
                  py="2"
                  minW="36"
                  cursor="pointer"
                  borderTopRadius={idx === 0 ? "lg" : "initial"}
                  borderBottomRadius={
                    idx === dropdownValue.length - 1 ? "lg" : "initial"
                  }
                  bg={selectedValue === item.title ? "gray.200" : "initial"}
                  _hover={{
                    bg: selectedValue !== item.title ? "gray.100" : "gray.200",
                  }}
                  onClick={() => handleSelectFilter(item)}
                >
                  {item.title}
                </Box>
              ))}
            </Flex>
          </SlideFade>
        </Box>
      </Flex>
    </Flex>
  );
}

export default Dropdown;
