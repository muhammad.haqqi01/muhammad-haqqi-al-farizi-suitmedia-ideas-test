import { BoxProps, Box } from "@chakra-ui/react";
import { ReactNode } from "react";

function ContentLayout(props: BoxProps): ReactNode {
  return (
    <Box {...props} px="24">
      {props.children}
    </Box>
  );
}

export default ContentLayout;
