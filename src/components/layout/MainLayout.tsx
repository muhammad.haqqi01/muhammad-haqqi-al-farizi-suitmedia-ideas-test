import { BoxProps, Flex } from "@chakra-ui/react";
import { ReactNode } from "react";
import Header from "../header";

function MainLayout(props: BoxProps): ReactNode {
  return (
    <Flex flexDirection="column" mb="16">
      <Header />
      <Flex {...props} w='full' minH="100vh" flexDirection="column">
        {props.children}
      </Flex>
    </Flex>
  );
}

export default MainLayout;
