import { Idea } from "@/types/api";
import { Box, Flex, Image, Text } from "@chakra-ui/react";
import { motion } from "framer-motion";
import { ReactNode } from "react";

function PostCard({ title, created_at, medium_image }: Idea): ReactNode {
  const date = new Intl.DateTimeFormat("id-ID", {
    dateStyle: "long",
  })
    .format(new Date(created_at))
    .toLocaleUpperCase();
  return (
    <Flex
      as={motion.div}
      position="relative"
      zIndex="2"
      flexDirection="column"
      borderRadius="lg"
      bg="white"
      boxShadow="lg"
      whileHover={{ scale: 1.02 }}
    >
      {medium_image.length > 0 ? (
        <Image
          h="160px"
          alt={title}
          src={medium_image[0].url}
          loading="lazy"
          objectFit="cover"
          borderTopRadius="lg"
        />
      ) : (
        <Box h="160px" />
      )}
      <Flex flexDirection="column" m="4">
        <Text fontSize="sm" fontWeight="medium" color="gray.400">
          {date}
        </Text>
        <Text fontWeight="bold" textOverflow="ellipsis" noOfLines={3}>
          {title}
        </Text>
      </Flex>
    </Flex>
  );
}

export default PostCard;
