const colorScheme = {
  suitmedia: {
    orange: "#FF6600",
    gray: "#666666",
  },
};

export default colorScheme;
