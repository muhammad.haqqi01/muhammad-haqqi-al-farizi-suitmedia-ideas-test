import config from "./config";
import colorScheme from "./colorScheme";

export { config, colorScheme };
