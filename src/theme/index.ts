import { extendTheme } from "@chakra-ui/react";
import { colorScheme, config } from "./foundations";

const overrides = {
  colors: colorScheme,
  config: config,
};

export default extendTheme(overrides);
