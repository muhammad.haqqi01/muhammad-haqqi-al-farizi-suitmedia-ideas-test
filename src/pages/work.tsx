import { MainLayout } from "@/components/layout";
import { Text } from "@chakra-ui/react";
import Head from "next/head";

export default function Work() {
  return (
    <>
      <Head>
        <title>Suitmedia - Work</title>
        <meta name="description" content="Suitmedia - Work" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <MainLayout justifyContent="center">
        <Text textAlign="center">Work</Text>
      </MainLayout>
    </>
  );
}
