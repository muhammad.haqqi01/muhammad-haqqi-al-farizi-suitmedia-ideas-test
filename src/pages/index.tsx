import { MainLayout } from "@/components/layout";
import { Text } from "@chakra-ui/react";
import Head from "next/head";

export default function Home() {
  return (
    <>
      <Head>
        <title>Suitmedia - Home</title>
        <meta name="description" content="Suitmedia - Home" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <MainLayout justifyContent="center">
        <Text textAlign="center">Home</Text>
      </MainLayout>
    </>
  );
}
