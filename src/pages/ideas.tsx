import Head from "next/head";
import { Banner, Dropdown, Pagination, PostCard } from "@/components";
import axios from "axios";
import { ideasProps } from "@/types/components";
import { useRouter } from "next/router";
import { Idea, IdeaResponse } from "@/types/api";
import { Flex, SimpleGrid, Text } from "@chakra-ui/react";
import { ContentLayout, MainLayout } from "@/components/layout";
import { filterTypeValue } from "@/types/constant";

export default function Ideas({ data }: ideasProps) {
  const router = useRouter();

  const pageNumberValues = [
    { title: "10", value: 10 },
    { title: "20", value: 20 },
    { title: "50", value: 50 },
  ];
  const sortValues = [
    { title: "Newest", value: "-published_at" },
    { title: "Oldest", value: "published_at" },
  ];

  function handleSelectFilter(type: filterTypeValue, item: number | string) {
    if (
      data.meta.per_page === item ||
      (type === "sort" &&
        router.query[type] === undefined &&
        item === "-published_at")
    )
      return;
    else {
      router.replace({
        pathname: "",
        query: { ...router.query, [type]: item },
      });
    }
  }

  const showedDataDescription = `Showing ${
    (data.meta.current_page - 1) * data.meta.per_page + 1
  } -
${
  data.meta.current_page * data.meta.per_page <= data.meta.total
    ? data.meta.current_page * data.meta.per_page
    : data.meta.total
}
  of ${data.meta.total}`;

  return (
    <>
      <Head>
        <title>Suitmedia - Ideas</title>
        <meta name="description" content="Suitmedia - Ideas Page" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <MainLayout>
        <Banner image={data.banner_image} />
        <ContentLayout pt="24" px="24">
          <Flex alignItems="center" justifyContent="space-between">
            <Text fontWeight="medium">{showedDataDescription}</Text>
            <Flex gap="8">
              <Dropdown
                title="Show per page"
                initDropdownTitle={data.meta.per_page.toString()}
                dropdownValue={pageNumberValues}
                onSelect={(selectedValue) =>
                  handleSelectFilter("page[size]", selectedValue)
                }
              />
              <Dropdown
                title="Sort by"
                initDropdownTitle={
                  router.query["sort"] === "published_at" ? "Oldest" : "Newest"
                }
                dropdownValue={sortValues}
                onSelect={(selectedValue) =>
                  handleSelectFilter("sort", selectedValue)
                }
              />
            </Flex>
          </Flex>
          <Pagination {...data.meta}>
            <SimpleGrid minChildWidth="240px" spacing="8" mt="4">
              {data.data.map((idea: Idea) => (
                <PostCard key={idea.id} {...idea} />
              ))}
            </SimpleGrid>
          </Pagination>
        </ContentLayout>
      </MainLayout>
    </>
  );
}

export async function getServerSideProps(ctx: any) {
  const { query } = ctx;

  // handle if user access invalid page < 1
  if (query["page[number]"] && query["page[number]"] < 1) {
    return {
      redirect: {
        destination: ctx.resolvedUrl.replace(
          `page%5Bnumber%5D=${query["page[number]"]}`,
          `page%5Bnumber%5D=1`
        ),
      },
    };
  }

  let { data }: { data: IdeaResponse } = await axios.get(
    `https://suitmedia-backend.suitdev.com/api/ideas`,
    {
      params: {
        "page[number]": query["page[number]"] ?? 1,
        "page[size]": query["page[size]"] ?? 10,
        "append[]": query["append[]"] ?? ["medium_image"],
        sort: query["sort"] ?? "-published_at",
      },
    }
  );

  // handle if user access more than last page
  if (data.meta.current_page > data.meta.last_page) {
    return {
      redirect: {
        destination: ctx.resolvedUrl.replace(
          `page%5Bnumber%5D=${query["page[number]"]}`,
          `page%5Bnumber%5D=${data.meta.last_page}`
        ),
      },
    };
  } else
    return {
      props: {
        data: {
          ...data,
          banner_image:
            "https://suitmedia.static-assets.id/storage/files/601/6.jpg",
        },
      },
    };
}
