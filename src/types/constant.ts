export type scrollValue = "bottom" | "up";
export type filterTypeValue = "page[size]" | "sort";

export type dropdownValue = {
  title: string;
  value: string | number;
};
