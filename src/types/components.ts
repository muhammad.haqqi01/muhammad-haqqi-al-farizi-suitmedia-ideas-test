import { BoxProps } from "@chakra-ui/react";
import { IdeaResponse, Meta } from "./api";
import { dropdownValue } from "./constant";

export interface layoutProps extends BoxProps {}

export interface ideasProps {
  data: IdeaResponse;
}

export interface bannerProps {
  image: string;
}

export interface paginationProps extends BoxProps, Meta {}

export interface dropdownProps {
  title: string;
  initDropdownTitle?: string;
  dropdownValue: dropdownValue[];
  onSelect: (selectedItem: number | string) => void;
}

export interface linkItemProps {
  title: string;
  url: string;
}
