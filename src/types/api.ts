export interface IdeaResponse {
  data: Idea[];
  links: PaginationUrls;
  meta: Meta;
  banner_image: string;
}

export interface Idea {
  id: number;
  slug: string;
  title: string;
  content: string;
  published_at: string;
  deleted_at: any;
  created_at: string;
  updated_at: string;
  small_image: Image[];
  medium_image: Image[];
}

export interface PaginationUrls {
  first: string;
  last: string;
  prev: any;
  next: string;
}

export interface Meta {
  current_page: number;
  from: number;
  last_page: number;
  links: Link[];
  path: string;
  per_page: number;
  to: number;
  total: number;
}

export interface Link {
  url?: string;
  label: string;
  active: boolean;
}

export interface Image {
  id: number;
  mime: string;
  file_name: string;
  url: string;
}
